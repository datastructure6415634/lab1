public class ArrayManipulation {
    public static void main(String[] args){
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names =  {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        //Print the elements of the "numbers" array using a for loop.
        System.out.println("numbers");
        for(int i=0;i<numbers.length;i++){
            System.out.println(numbers[i]);
        }
        System.out.println("");

        //Print the elements of the "names" array using a for-each loop.
        System.out.println("");
        System.out.println("names");
        for(int i=0;i<names.length;i++){
            System.out.println(names[i]);
        }
        System.out.println("");

        //Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0]=1.1;
        values[1]=1.2;
        values[2]=1.3;
        values[3]=1.4;

        //Calculate and print the sum of all elements in the "numbers" array.
        int sum=0;
        for(int i=0;i<numbers.length;i++){
            sum = sum + numbers[i];
        }
        System.out.println("sum number = "+sum);
        System.out.println("");

        //Find and print the maximum value in the "values" array.
        double max = 0.0;
        for(int i=0;i<values.length;i++){
            if(values[i]>max){
                max = values[i];
            }

        }
        System.out.println("the maximum value"+max);
        System.out.println("");

        //Create a new string array named "reversedNames"
        //Fill the "reversedNames" array with the elements of the "names" array in reverse order.
        String[] reversedNames = new String[names.length];

        System.out.println("reversedNames");
        for(int i=0;i<names.length;i++){
            reversedNames[i] = names[names.length-1-i];

            //Print the elements of the "reversedNames" array.
            System.out.println(reversedNames[i]);
        }
        
    }

}
