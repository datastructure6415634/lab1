public class SearchArray {

    static int searchArray(int[] x,int n){
        int r = x.length - 1;
        int l = 0;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            
            if (x[mid] == n) {
                return mid;
            }

            if (x[l] <= x[mid]) {
                if (x[l] <= n && n < x[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            } 
             else {
                if (x[mid] < n && n <= x[r]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) throws Exception {
       
        int [] nums = {4,5,6,7,0,1,2};
        int target = 0;
        System.out.println(searchArray(nums, target));
    }
}


