import java.util.Scanner;
public class Seat {

    Scanner sc = new Scanner(System.in);
    static int total;
    private int numSeat;
    private int price = 120;
    private int priceVip = 250; 

    private String[][] seat = {
        {"A1","A2","A3","A4","A5","A6","A7","A8","A9","A10"},
        {"B1","B2","B3","B4","B5","B6","B7","B8","B9","B10"},
        {"C1","C2","C3","C4","C5","C6","C7","C8","C9","C10"}};

    private  String[] seatVip = {"P1","P2","P3","P4","P5","P6"};

    public void setTotal(int total){
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    private String[] s;

    public void setS(int numSeat) {
        this.s = new String[numSeat];
    }

    public String[] getS() {
        return s;
    }

    public void setInputnumSeat(int numSeat) {
        this.numSeat = numSeat;
        setS(numSeat);

    }

    public void inputNumSeat() {
        System.out.println("------------------------------");
        System.out.print("Please input Number of Seats : ");
        numSeat = sc.nextInt();
        setInputnumSeat(numSeat);
    }

    public int getNumSeat() {
        return numSeat;
    }

    public void showSeat() {
        for (int i = 0; i < 3; i++) {
        System.out.print("| ");
        for (int j = 0; j < 10; j++) {
            System.out.print(seat[i][j]+" ");
        }
        System.out.println(" |");
    }
    System.out.print("      ");
    System.out.print("|");
    for (int i = 0; i < 6; i++) {
        System.out.print(" "+seatVip[i]);
    }
    System.out.println(" |");

    }

    public void setSeat() {
        for (int i = 0; i < numSeat; i++) {
            for (int row = 0; row < seat.length; row++) {
                for (int col = 0; col < seat[row].length; col++) {
                    if (seat[row][col].equals(s[i])) {
                        seat[row][col] = "XX";
                        total = total+price;
                    }
                }
            }
        }

        for (int i = 0; i < numSeat; i++) {
            for (int j = 0; j < seatVip.length; j++) {
                    if (seatVip[j].equals(s[i])) {
                        seatVip[j] = "XX";
                        total = total+priceVip;
                    }
                
            }
        }

    }

    public void showPrice(){
        for (int i = 0; i < numSeat; i++) {
            for (int row = 0; row < seat.length; row++) {
                for (int col = 0; col < seat[row].length; col++) {
                    if (seat[row][col].equals(s[i])) {
                        System.out.println(s[i]);
                    }
                }
            }
        }

        for (int i = 0; i < numSeat; i++) {
            for (int j = 0; j < seatVip.length; j++) {
                    if (seatVip[j].equals(s[i])) {
                        System.out.println(s[i]+" = "+priceVip+" THB");
                    }
                
            } 
        }
    }
   

    public void chooseSeat() {
        inputNumSeat();
        System.out.println("Row V 250 THB\nOther row 120 THB");
        System.out.println("");
        showSeat();

        for (int i = 0; i < numSeat; i++) {
            System.out.print("Please choose seat" + (i + 1) + " : ");
            s[i] = sc.next();
            setSeat();
        }
        System.out.println("");
        getSeat();
        showSeat();
        printSeat();
        
        showPrice();
        printTotal();
        
    }


    public String[][] getSeat() {
       return seat;
    }
    
    public String[] getSeatVip() {
       return seatVip;
    }

    public void printSeat() {
       System.out.print("Your Seat : ");
        for (int i = 0; i < numSeat; i++) {
            System.out.print(s[i] + " ");

        }
        System.out.println();
    }
    public void printTotal() {
        System.out.println("Total : "+total);
    }

}
